# bbv-cli

CLI client for bigbigvpn. indev.

## OS Support for Automated VPN management

- MacOS, with [CLI wireguard](https://ports.macports.org/port/wireguard-tools/summary) (tested on Intel Big Sur)
- Linux (tested on Void Linux)
- Windows (tested on Windows 10 20H2)

**Note:** It is possible to generate WireGuard .conf files in other OSes too, but bbv-cli may not be able to automatically connect or disconnect you. Similarly, it may not be possible to generate WireGuard keys directly in BBV on OSes not listed here.

## Installing and using

Requirements:

- Python 3.9+
- WireGuard®

```
git clone https://gitlab.com/bigbigvpn/bbv-cli
cd bbv-cli
python3.9 -m pip install -Ur requirements.txt
python3.9 main.py
```

## Example usage

```
# registering
$ python3 main.py -s http://localhost:6900 auth register -e l@l4.pm -u luna

# logging in
$ python3 main.py -s http://localhost:6900 auth login -u luna -p 123

# seeing own info after logging in
$ python3 main.py user atme
```
