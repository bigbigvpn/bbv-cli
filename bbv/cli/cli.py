import json
import os
from pathlib import Path

import click
from halo import Halo
import appdirs

from ..api.client import Client, ClientError
from .auth import auth
from .user import user
from .group import group
from .vpn import vpn
from .cli_state import CliState


CONFIG_PATH = os.path.join(appdirs.user_config_dir(appname="bbv"), "cli.json")
DEFAULT_CONFIG = {
    "state": CliState.NOT_LOGGED_IN,
    "api_base_url": None,
    "token": None,
}


@click.group()
@click.pass_context
@click.option(
    "-s",
    "--server",
    help="BBV server",
    default=None,
)
def entry_point(ctx, server):
    cfg_path = Path(os.environ.get("BBV_CONFIG_PATH", CONFIG_PATH)).expanduser()
    cfg_folder = cfg_path.parent
    cfg_folder.mkdir(exist_ok=True, parents=True)

    ctx.cfg_path = cfg_path
    ctx.cfg_folder = cfg_folder

    try:
        with cfg_path.open(mode="r") as fp:
            cfg = json.load(fp)
    except FileNotFoundError:
        cfg = DEFAULT_CONFIG
        with cfg_path.open(mode="w") as fp:
            json.dump(DEFAULT_CONFIG, fp)

        print(f"Config file written to {cfg_path}")

    ctx.cfg = cfg
    ctx.spinner = Halo(spinner="dots")
    if server is not None:
        ctx.cfg["api_base_url"] = server
    ctx.client = Client(base_url=cfg.get("api_base_url"), token=cfg.get("token"))

    if ctx.client.base_url is None:
        print("missing server url. add '-s ..' parameter to invocation")
        ctx.exit(1)


@entry_point.command(hidden=True)
@click.pass_context
def notnite(ctx):
    click.echo('"WireGuard" is a registered trademark of Jason A. Donenfeld.')


entry_point.add_command(auth)
entry_point.add_command(user)
entry_point.add_command(group)
entry_point.add_command(vpn)

if __name__ == "__main__":
    entry_point()
