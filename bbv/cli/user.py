import click
import subprocess
import shutil
from .util import SpinnerExecutor, save_config
from bbv.api.client import ClientError


@click.group(help="User options")
@click.pass_context
def user(ctx):
    return


@user.command(help="Get user info")
@click.pass_context
def atme(ctx):
    ctx = ctx.parent.parent
    with SpinnerExecutor(
        ctx, "Fetching account info", success_text="Fetched account info"
    ) as executor:
        myself = ctx.client.myself()
    click.echo(repr(myself))


@user.group(help="Wireguard related commands")
@click.pass_context
def wireguard(ctx):
    return


@wireguard.command(name="list", help="List all wireguard keys for the current user")
@click.pass_context
def wireguard_list(ctx):
    ctx = ctx.parent.parent.parent
    with SpinnerExecutor(
        ctx, "Fetching keys", success_text="Fetched keys successfully."
    ) as executor:
        keys = ctx.client.list_wg_keys()
    click.echo(repr(keys))


@wireguard.command(
    name="gen", help="Automatically generate and add a WireGuard® key to the user"
)
@click.password_option(
    confirmation_prompt=False, help="Account password", required=True
)
@click.pass_context
def wireguard_gen(ctx, password):
    ctx = ctx.parent.parent.parent
    if not shutil.which("wg"):
        click.echo('"wg" is not in your PATH. Try installing wireguard-tools.')
        return

    with SpinnerExecutor(
        ctx, "Generating key", success_text="Generated key"
    ) as executor:
        privkey = subprocess.run(
            ["wg", "genkey"], check=True, capture_output=True, text=True
        ).stdout.strip()

        pubkey = subprocess.run(
            ["wg", "pubkey"], input=privkey, check=True, capture_output=True, text=True
        ).stdout.strip()

    with SpinnerExecutor(ctx, "Adding key", success_text="Added key") as executor:
        keys = ctx.client.add_wg_key(pubkey, password)

    key_data = [
        key for key in keys["wireguard_keys"] if key["wireguard_public_key"] == pubkey
    ][0]

    ctx.cfg["wg"] = {"priv": privkey, "pub": pubkey, "key_data": key_data}
    save_config(ctx)
    click.echo(repr(keys))


@wireguard.command(name="add", help="Add a WireGuard® key to the user")
@click.option("-pub", "--pubkey", help="The public key to be added", required=True)
@click.option(
    "-priv", "--privkey", help="The private key to be locally stored", required=True
)
@click.password_option(
    confirmation_prompt=False, help="Account password", required=True
)
@click.pass_context
def wireguard_add(ctx, pubkey, privkey, password):
    ctx = ctx.parent.parent.parent
    with SpinnerExecutor(ctx, "Adding key", success_text="Added key") as executor:
        keys = ctx.client.add_wg_key(pubkey, password)

    key_data = [
        key for key in keys["wireguard_keys"] if key["wireguard_public_key"] == pubkey
    ][0]

    ctx.cfg["wg"] = {"priv": privkey, "pub": pubkey, "key_data": key_data}

    save_config(ctx)
    click.echo(repr(keys))


@wireguard.command(name="rm", help="Remnove a WireGuard® key from the user")
@click.option("-pk", "--pubkey", help="The public key to be removed", required=True)
@click.password_option(confirmation_prompt=False)
@click.pass_context
def wireguard_rm(ctx, pubkey, password):
    ctx = ctx.parent.parent.parent
    with SpinnerExecutor(
        ctx, "Removing wireguard key", success_text="Removed wireguard key"
    ) as executor:
        keys = ctx.client.rm_wg_key(pubkey, password)

    # Remove local wg key if we just removed it from server
    if pubkey == ctx.cfg["wg"].get("pub"):
        del ctx.cfg["wg"]
        save_config(ctx)

    click.echo(repr(keys))
