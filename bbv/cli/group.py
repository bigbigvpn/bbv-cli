import click
from .util import SpinnerExecutor, save_config, get_group_id
from bbv.api.client import ClientError


def set_default_group(ctx, group_data: dict, only_if_not_in_one: bool = False):
    needs_save = False
    if "defaults" not in ctx.cfg:
        ctx.cfg["defaults"] = {}
        needs_save = True

    has_no_default_group = "group" not in ctx.cfg["defaults"]
    if has_no_default_group or not only_if_not_in_one:
        with SpinnerExecutor(ctx, "Setting default group", "Set default group"):
            ctx.cfg["defaults"]["group"] = group_data
        needs_save = True

    if needs_save:
        save_config(ctx)


def unset_default_group(ctx, only_if_matches_id=None):
    needs_save = False
    if "defaults" not in ctx.cfg:
        ctx.cfg["defaults"] = {}
        needs_save = True

    has_default_group = "group" in ctx.cfg["defaults"]

    matches_id = True
    if has_default_group and only_if_matches_id:
        matches_id = ctx.cfg["defaults"]["group"]["id"] == only_if_matches_id

    if has_default_group and matches_id:
        with SpinnerExecutor(ctx, "Unsetting default group", "Unset default group"):
            del ctx.cfg["defaults"]["group"]
        needs_save = True

    if needs_save:
        save_config(ctx)


@click.group(help="Group commands")
@click.pass_context
def group(ctx):
    return


@group.command(name="create", help="Create a group")
@click.option("-n", "--name", help="The group's name", required=True)
@click.pass_context
def create_group(ctx, name):
    ctx = ctx.parent.parent
    with SpinnerExecutor(ctx, "Creating group"):
        group_data = ctx.client.create_group(name)
        set_default_group(ctx, group_data, only_if_not_in_one=True)
    click.echo(repr(group_data))


@group.command(name="delete", help="Delete a group")
@click.option("-g", "--group", help="The group's UUID", default=None)
@click.option(
    "-p",
    "--password",
    help="User password",
    required=True,
    prompt=True,
    hide_input=True,
)
@click.confirmation_option()
@click.pass_context
def delete_group(ctx, group, password):
    ctx = ctx.parent.parent
    group = get_group_id(ctx, group)
    with SpinnerExecutor(ctx, "Deleting group", "Deleted group"):
        ctx.client.delete_group(group, password)
        unset_default_group(ctx, only_if_matches_id=group)


@group.command(name="leave", help="Leave a group")
@click.option("-g", "--group", help="The group's UUID", default=None)
@click.confirmation_option()
@click.pass_context
def leave_group(ctx, group):
    ctx = ctx.parent.parent
    group = get_group_id(ctx, group)
    with SpinnerExecutor(ctx, "Leaving group", "Left group"):
        ctx.client.leave_group(group)
        unset_default_group(ctx, only_if_matches_id=group)


@group.command(name="list", help="List groups")
@click.pass_context
def list_groups(ctx):
    ctx = ctx.parent.parent
    with SpinnerExecutor(ctx, "Fetching group list", "Fetched group list"):
        groups = ctx.client.list_groups()
    click.echo(repr(groups))


@group.group(help="Invite commands")
@click.pass_context
def invite(ctx):
    return


@invite.command(name="create", help="Create an invite")
@click.option("-g", "--group", help="The group's UUID", default=None)
@click.option("-m", "--max-uses", help="Max amount of uses", default=None)
@click.option("-e", "--expires-at", help="Expiration timestamp (ISO8601)", default=None)
@click.pass_context
def create_invite(ctx, group, max_uses, expires_at):
    ctx = ctx.parent.parent.parent
    group = get_group_id(ctx, group)
    with SpinnerExecutor(ctx, "Creating Invite"):
        group_data = ctx.client.create_invite(group, max_uses, expires_at)
    click.echo(repr(group_data))


@invite.command(name="list", help="List invites in a group")
@click.option("-g", "--group", help="The group's UUID", default=None)
@click.pass_context
def list_invites(ctx, group):
    ctx = ctx.parent.parent.parent
    group = get_group_id(ctx, group)
    with SpinnerExecutor(ctx, "Fetching invites"):
        group_data = ctx.client.list_invites(group)
    click.echo(repr(group_data))


@invite.command(name="delete", help="Delete an invite")
@click.option("-g", "--group", help="The group's UUID", default=None)
@click.option("-i", "--invite", help="The invite to be deleted", required=True)
@click.pass_context
def delete_invite(ctx, group, invite):
    ctx = ctx.parent.parent.parent
    group = get_group_id(ctx, group)
    with SpinnerExecutor(ctx, "Deleting invite"):
        group_data = ctx.client.delete_invite(group, invite)
    click.echo(repr(group_data))


@invite.command(name="use", help="Use an invite")
@click.option("-i", "--invite", help="The invite to be used", required=True)
@click.pass_context
def use_invite(ctx, invite):
    ctx = ctx.parent.parent.parent
    with SpinnerExecutor(ctx, "Using invite"):
        group_data = ctx.client.use_invite(invite)
        set_default_group(ctx, group_data, only_if_not_in_one=True)
    click.echo(repr(group_data))


@group.group(name="default", help="Manage default group")
@click.pass_context
def default(ctx):
    return


@default.command(name="set", help="Set your default group")
@click.option("-g", "--group", help="The group's UUID", required=True)
@click.pass_context
def set_default(ctx, group):
    ctx = ctx.parent.parent.parent

    with SpinnerExecutor(ctx, "Fetching group data", "Fetched group data"):
        group_data = ctx.client.fetch_group(group)

    # TODO: Temporary until we fix exception bubbling up
    if not group_data:
        return

    set_default_group(ctx, group_data, only_if_not_in_one=False)


@default.command(name="unset", help="Unset your default group")
@click.pass_context
def unset_default(ctx):
    ctx = ctx.parent.parent.parent
    unset_default_group(ctx)


@default.command(name="get", help="Get your default group")
@click.pass_context
def get_default(ctx):
    ctx = ctx.parent.parent.parent
    if "defaults" not in ctx.cfg:
        ctx.cfg["defaults"] = {}
        save_config(ctx)

    if "group" not in ctx.cfg["defaults"]:
        return click.echo("You don't have a default group!")

    click.echo(f'Default group: {ctx.cfg["defaults"]["group"]}')


@group.group(name="token", help="Cloud token commands")
@click.pass_context
def token(ctx):
    return


@token.command(name="list", help="List all cloud tokens in a group")
@click.option("-g", "--group", help="The group's UUID", default=None)
@click.pass_context
def list_tokens(ctx, group):
    ctx = ctx.parent.parent.parent
    group = get_group_id(ctx, group)
    with SpinnerExecutor(ctx, "Fetching tokens"):
        group_data = ctx.client.fetch_cloud_tokens(group)
    click.echo(repr(group_data))


@token.command(name="set", help="Set a cloud token for a group")
@click.option("-g", "--group", help="The group's UUID", default=None)
@click.option(
    "-t",
    "--type",
    help="Cloud provider type (digitalocean, hetzner, etc)",
    required=True,
)
@click.option(
    "-v", "--value", help="The actual API token from the cloud provider", required=True
)
@click.pass_context
def set_token(ctx, group, type, value):
    ctx = ctx.parent.parent.parent
    group = get_group_id(ctx, group)
    with SpinnerExecutor(ctx, "Updating tokens"):
        result = ctx.client.set_cloud_token(group, type, value)
    click.echo(repr(result))


@token.command(name="unset", help="Unset/remove a cloud token for a group")
@click.option("-g", "--group", help="The group's UUID", default=None)
@click.option(
    "-t",
    "--type",
    help="Cloud provider type (digitalocean, hetzner, etc)",
    required=True,
)
@click.pass_context
def unset_token(ctx, group, type):
    ctx = ctx.parent.parent.parent
    group = get_group_id(ctx, group)
    with SpinnerExecutor(ctx, "Updating tokens"):
        result = ctx.client.unset_cloud_token(group, type)
    click.echo(repr(result))
