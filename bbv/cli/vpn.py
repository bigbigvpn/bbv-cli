import click
import ctypes
import subprocess
import shutil
import httpx
import time
import tempfile
import platform
import os
from pathlib import Path
from .util import (
    SpinnerExecutor,
    generate_wireguard_config,
    save_config,
    cidr_to_ipv4,
    cidr_to_ipv6,
    get_group_id,
)


@click.group(help="VPN commands")
@click.pass_context
def vpn(ctx):
    return


def get_server_and_group_id(
    ctx, server_argument, group_argument, allow_last=True
) -> str:
    if server_argument and group_argument:
        # TODO: Ideally this false should be if these
        # two vars match their last_connection equivalents
        return server_argument, group_argument, False
    elif not server_argument and group_argument:
        if allow_last:
            click.echo(
                "Please provide both -g and -s, or neither -g or -s to use connected server, or just -s to use default group."
            )
        else:
            click.echo(
                "Please provide both -g and -s, or just -s to use default group."
            )
        ctx.exit(3)
    elif server_argument and not group_argument:
        # TODO: Ideally this false should be if these
        # two vars match their last_connection equivalents
        return server_argument, get_group_id(ctx, None), False
    elif ("last_connection" in ctx.cfg) and allow_last:
        return (
            ctx.cfg["last_connection"]["server_id"],
            ctx.cfg["last_connection"]["group_id"],
            True,
        )
    else:
        click.echo("Missing server ID. Add '-s ..' parameter to invocation.")
        ctx.exit(4)


def path_to_interface(config_path: str) -> str:
    return os.path.splitext(os.path.basename(config_path))[0]


def is_unixy():
    return platform.system() in ["Darwin", "Linux"]


def is_netbsd():
    # Note: only versions above 10 work
    return platform.system() == "NetBSD" and "10.0_RC1" in platform.version()


def is_windows():
    return platform.system() == "Windows"


def needs_sudo() -> bool:
    # Non-unix doesn't need sudo, tho might need smth else
    if not (is_unixy() or is_netbsd()):
        return False

    # If we're root, then we don't need sudo
    if os.getuid == 0:
        return False

    # If we don't have sudo then we can't use it anyways
    if not shutil.which("sudo"):
        return False

    return True


def needs_uac() -> bool:
    # Windll isn't available under non-Windows
    if not is_windows():
        return False

    is_admin = bool(ctypes.windll.shell32.IsUserAnAdmin())

    return not is_admin


def get_sudo():
    if not needs_sudo():
        return None
    sudo_returncode = subprocess.run(["sudo", "-v"], text=True).returncode
    while not sudo_returncode == 0:
        sudo_returncode = subprocess.run(["sudo", "-v"], text=True).returncode


def is_connected(interface_name: str) -> bool:
    current_platform = platform.system()
    if current_platform == "Linux":
        return os.path.exists(f"/sys/class/net/{interface_name}")
    elif current_platform == "Darwin":
        return os.path.exists(f"/var/run/wireguard/{interface_name}.name")
    elif current_platform == "Windows":
        return (
            subprocess.run(
                ["powershell", "-command", f"Get-NetAdapter {interface_name}"],
                capture_output=True,
            ).returncode
            == 0
        )
    else:
        # TODO: We assume no in this case.
        return False


def wg_quick_path():
    if is_unixy():
        return "wg-quick"
    if is_netbsd():
        return "wg4-quick"
    raise NotImplementedError("Unsupported OS for wg-quick path")


def wg_present():
    if is_windows():
        if not shutil.which("wireguard"):
            click.echo('"wireguard" is not in your PATH. Try installing wireguard.')
            return False
    elif is_unixy():
        if not shutil.which("wg-quick"):
            click.echo(
                '"wg-quick" is not in your PATH. Try installing wireguard-tools.'
            )
            return False
    elif is_netbsd():
        if not shutil.which("wg4-quick"):
            click.echo(
                '"wg4-quick" is not in your PATH. Install wg4-quick from the repository.'
            )
            return False
    else:
        raise NotImplementedError(
            f"Your system ({platform.system()} {platform.version()}) is not supported by bbv-cli."
        )
    return True


def wg_connect(ctx, config_path: str):
    if not wg_present():
        raise NotImplementedError()

    get_sudo()

    with SpinnerExecutor(ctx, "Connecting to VPN...", "Connected successfully."):
        if is_windows():
            wg_command = ["wireguard", "/installtunnelservice", config_path]
            # powershell -Command "Start-Process 'wireguard /installtunnelservice path' -Verb runAs"
            if needs_uac():
                # start-process -verb runAs wireguard "/installtunnelservice C:\Users\Ave\AppData\Local\Temp\bbvhgyrbkz_.conf"
                wg_command = [
                    "powershell",
                    "-command",
                    f'Start-Process -verb runAs wireguard "/installtunnelservice {config_path}"',
                ]
            subprocess.run(wg_command)
        elif is_unixy() or is_netbsd():
            wg_command = [wg_quick_path(), "up", config_path]
            if needs_sudo():
                wg_command = ["sudo"] + wg_command

            subprocess.run(wg_command, capture_output=True, check=True)
        else:
            raise NotImplementedError()


def wg_disconnect(ctx, config_path: str):
    if not is_unixy() and not is_windows():
        raise NotImplementedError()

    get_sudo()

    with SpinnerExecutor(
        ctx, "Disconnecting from VPN...", "Disconnected successfully."
    ):
        if is_windows():
            interface_name = path_to_interface(config_path)
            wg_command = ["wireguard", "/uninstalltunnelservice", interface_name]
            # powershell -Command "Start-Process 'wireguard /uninstalltunnelservice interface' -Verb runAs"
            if needs_uac():
                # start-process -verb runAs wireguard "/uninstalltunnelservice bbvhgyrbkz_"
                wg_command = [
                    "powershell",
                    "-command",
                    f'Start-Process -verb runAs wireguard "/uninstalltunnelservice {interface_name}"',
                ]
            subprocess.run(wg_command)
        elif is_unixy() or is_netbsd():
            wg_command = [wg_quick_path(), "down", config_path]
            if needs_sudo():
                wg_command = ["sudo"] + wg_command

            subprocess.run(wg_command, text=True, capture_output=True)


def vpn_genconfig(ctx, vpn_state, local):
    wg_config = generate_wireguard_config(vpn_state, ctx.cfg["wg"], local)
    wg_fd, wg_path = tempfile.mkstemp(prefix="bbv", suffix=".conf", text=True)
    with os.fdopen(wg_fd, "w") as f:
        f.write(wg_config)
    return wg_path


def vpn_connect(ctx, vpn_state, local, group):
    wg_path = vpn_genconfig(ctx, vpn_state, local)
    wg_connect(ctx, wg_path)

    vpn_ips = [cidr_to_ipv4(vpn_state["ipv4_network"])]
    if vpn_state.get("ipv6_network"):
        vpn_ips.append(cidr_to_ipv6(vpn_state["ipv6_network"]))

    # Determine interface name through the filename
    interface_name = path_to_interface(wg_path)

    ctx.cfg["last_connection"] = {
        "interface": interface_name,
        "path": wg_path,
        "group_id": group,
        "server_id": vpn_state["id"],
    }
    save_config(ctx)

    local_ips = [
        ctx.cfg["wg"]["key_data"]["local_ipv4_address"],
        ctx.cfg["wg"]["key_data"]["local_ipv6_address"],
    ]

    if local:
        click.echo(f"You're now connected to BBV intranet on {vpn_state['hostname']}.")
    else:
        # Verify connection by comparing our IP on ipdata.top to expected values
        with SpinnerExecutor(ctx, "Verifying connection...", "Connection verified"):
            retries = 0
            if retries > 10:
                click.echo("failed to verify that IP was set to the VPN server's ip...")
                return
            detected_ip = None
            while detected_ip not in vpn_ips:
                try:
                    req = httpx.get("https://ipdata.top/json")
                    detected_ip = req.json().get("ip")
                    retries += 1
                except:
                    # TODO: bad practices
                    pass
        click.echo(f"Your traffic is now routed through {', '.join(vpn_ips)}.")

    click.echo(f"Your local IPs: {', '.join(local_ips)}")


# TODO: Do we really need to get both type and region? check docs
@vpn.command(name="summon", help="Summons a server and optionally connects to VPN")
@click.option("-g", "--group", help="The group's UUID", default=None)
@click.option(
    "-t",
    "--type",
    help="Cloud provider type (digitalocean, hetzner, etc)",
    required=True,
)
@click.option(
    "-r",
    "--region",
    help="Cloud provider region (hel1, fra1, etc)",
    required=True,
)
@click.option(
    "-p",
    "--port",
    help="Port for WireGuard server (53, 443, etc)",
    default=53,
)
@click.option(
    "--local", help="Use VPN for BBV intranet only", is_flag=True, default=False
)
@click.option(
    "--noconnect",
    help="Don't connect to the summoned VPN, just generate config",
    is_flag=True,
    default=False,
)
@click.pass_context
def summon(ctx, group, type, region, port, local, noconnect):
    ctx = ctx.parent.parent
    group = get_group_id(ctx, group)

    # Verify that user isn't already connected
    if "last_connection" in ctx.cfg and is_connected(
        ctx.cfg["last_connection"]["interface"]
    ):
        click.echo("You're already connected to a BBV node.")
        return

    # Verify that user has a wireguard key
    if "wg" not in ctx.cfg:
        click.echo("Can't find a wireguard key!")
        click.echo('Use "user wireguard" commands to add a key first!')
        return

    # Verify that user has a wireguard installed if we'll connect
    if not noconnect:
        wireguard_present = wg_present()
        if not wireguard_present:
            return

    with SpinnerExecutor(ctx, "Summoning server...", "Summoned server."):
        vpn_state = ctx.client.summon_server(group, type, region, port)["vpn"]

    with SpinnerExecutor(ctx, "Waiting for server to get ready...", "Server is ready."):
        while vpn_state["state"] != "summoned":
            vpn_state = ctx.client.fetch_server(group, vpn_state["id"])["vpn"]
            time.sleep(1)

    if noconnect:
        wg_path = vpn_genconfig(ctx, vpn_state, local)
        click.echo(f"WireGuard config path: {wg_path}")
    else:
        vpn_connect(ctx, vpn_state, local, group)


def vpn_disconnect(ctx):
    # Verify that user isn't already connected
    if "last_connection" not in ctx.cfg:
        click.echo("You're not connected to a BBV node.")
        return False

    # Verify that user has a wireguard installed
    wireguard_present = wg_present()
    if not wireguard_present:
        return

    wg_path = ctx.cfg["last_connection"]["path"]

    if is_connected(ctx.cfg["last_connection"]["interface"]):
        wg_disconnect(ctx, wg_path)

    if os.path.exists(wg_path):
        os.unlink(wg_path)
    return True


@vpn.command(name="disconnect", help="Disconnects from active VPN session")
@click.pass_context
def disconnect(ctx):
    ctx = ctx.parent.parent
    vpn_disconnect(ctx)

    del ctx.cfg["last_connection"]
    save_config(ctx)


@vpn.command(name="list", help="List available VPN servers")
@click.option("-g", "--group", help="The group's UUID", default=None)
@click.pass_context
def list_servers(ctx, group):
    ctx = ctx.parent.parent
    group = get_group_id(ctx, group)

    with SpinnerExecutor(ctx, "Fetching server list...", "Fetched server list."):
        servers = ctx.client.fetch_servers(group)

    click.echo(repr(servers))


@vpn.command(name="unsummon", help="Unsummon a VPN server")
@click.option(
    "-g",
    "--group",
    help="The group's UUID (defaults to connected server's group)",
    default=None,
)
@click.option(
    "-s",
    "--server",
    help="The server's UUID (defaults to connected server)",
    default=None,
)
@click.pass_context
def unsummon(ctx, server, group):
    ctx = ctx.parent.parent
    server, group, is_last_conn = get_server_and_group_id(ctx, server, group)
    if is_last_conn:
        vpn_disconnect(ctx)
        del ctx.cfg["last_connection"]
        save_config(ctx)

    with SpinnerExecutor(ctx, "Unsummoning server...", "Unsummoned server."):
        ctx.client.unsummon_server(group, server)


@vpn.command(name="connect", help="Connect to a running VPN server")
@click.option(
    "-g",
    "--group",
    help="The group's UUID",
    default=None,
)
@click.option(
    "-s",
    "--server",
    help="The server's UUID",
    default=None,
)
@click.option(
    "--local", help="Use VPN for BBV intranet only", is_flag=True, default=False
)
@click.pass_context
def connect(ctx, server, group, local):
    ctx = ctx.parent.parent
    server, group, is_last_conn = get_server_and_group_id(
        ctx, server, group, allow_last=False
    )

    vpn_state = ctx.client.fetch_server(group, server)["vpn"]
    vpn_connect(ctx, vpn_state, local, group)


@vpn.command(
    name="genconfig", help="Generate a WireGuard config of a running VPN server"
)
@click.option(
    "-g",
    "--group",
    help="The group's UUID",
    default=None,
)
@click.option(
    "-s",
    "--server",
    help="The server's UUID",
    default=None,
)
@click.option(
    "--local", help="Use VPN for BBV intranet only", is_flag=True, default=False
)
@click.pass_context
def genconfig(ctx, server, group, local):
    ctx = ctx.parent.parent
    server, group, is_last_conn = get_server_and_group_id(
        ctx, server, group, allow_last=False
    )

    vpn_state = ctx.client.fetch_server(group, server)["vpn"]
    wg_path = vpn_genconfig(ctx, vpn_state, local)
    click.echo(f"WireGuard config path: {wg_path}")
