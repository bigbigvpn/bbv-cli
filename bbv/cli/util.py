from bbv.api.client import ClientError
import os
import json
import click
import shutil
import tempfile
import ipaddress


class SpinnerExecutor:
    def __init__(self, ctx, action, success_text="Success!"):
        self.ctx = ctx
        self.action = action
        self.success_text = success_text

    def __enter__(self):
        self.ctx.spinner.start(self.action)

    def __exit__(self, typ, val, _traceback):
        # if typ is not ClientError:
        #     return False

        if typ is None:
            self.ctx.spinner.stop_and_persist(symbol="✅", text=self.success_text)
        else:
            self.ctx.spinner.stop_and_persist(
                symbol="❌",
                text=f"Failed: {val!r}",
            )

        return True


def save_config(ctx):
    """Atomically save the configuration file.

    This achieves atomicity by creating a temporary file, writing the config
    to it, then moving it to the actual config path (pointed by ctx.cfg_path).

    If serialization of the config data fails, only the temporary file will
    be affected and CLI usage can still continue.

    From https://man7.org/linux/man-pages/man2/rename.2.html:
       If newpath already exists, it will be atomically replaced, so
       that there is no point at which another process attempting to
       access newpath will find it missing.  However, there will
       probably be a window in which both oldpath and newpath refer to
       the file being renamed.
    """
    ctx.spinner.start("Saving to config...")

    # we use ctx.cfg_folder because we can't rename files across filesystems
    # and the default directory, /tmp, is on tmpfs.
    temp_fd, temp_path = tempfile.mkstemp(
        dir=ctx.cfg_folder,
        prefix="bbv-cli-temp-conf",
        suffix=".json",
        text=True,
    )
    with os.fdopen(temp_fd, mode="w") as fp:
        json.dump(ctx.cfg, fp)

    try:
        shutil.move(temp_path, ctx.cfg_path)
    finally:
        # always attempt to clean our temporary file
        try:
            os.unlink(temp_path)
        except FileNotFoundError:
            pass

    ctx.spinner.stop_and_persist(symbol="✅", text=f"Saved config.")


def cidr_to_ipv4(cidr: str) -> str:
    return str(ipaddress.IPv4Network(cidr).network_address)


def cidr_to_ipv6(cidr: str) -> str:
    return str(ipaddress.IPv6Network(cidr).network_address)


def generate_wireguard_config(vpn_state, wg_object, local=False) -> str:
    server_ipv4 = cidr_to_ipv4(vpn_state["ipv4_network"])
    allowed_ips = "172.16.0.0/12, fd00:b00b::/32" if local else "0.0.0.0/0, ::/0"
    wg_config = f"""
[Interface]
Address = {wg_object['key_data']['local_ipv4_address']}, {wg_object['key_data']['local_ipv6_address']}
PrivateKey = {wg_object['priv']}

[Peer]
PublicKey = {vpn_state['wireguard_public_key']}
Endpoint = {server_ipv4}:{vpn_state['wireguard_port']}
AllowedIPs = {allowed_ips}
PersistentKeepalive = 20
""".strip()
    return wg_config


def get_group_id(ctx, group_argument) -> str:
    if group_argument:
        return group_argument
    elif "defaults" in ctx.cfg and "group" in ctx.cfg["defaults"]:
        return ctx.cfg["defaults"]["group"]["id"]
    else:
        click.echo("Missing group ID. Add '-g ..' parameter to invocation.")
        click.echo(
            'You may alternatively set a default with "group default set" command.'
        )
        ctx.exit(2)
