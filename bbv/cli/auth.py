from pathlib import Path

import click

from bbv.api.client import ClientError
from .cli_state import CliState
from .util import SpinnerExecutor, save_config


@click.group(help="Authentication commands")
@click.pass_context
def auth(ctx):
    return


@auth.command(help="Register to BBV")
@click.pass_context
@click.option("-e", "--email", help="Email", required=True)
@click.option("-u", "--username", help="Username", required=True)
@click.password_option()
def register(ctx, email: str, username: str, password: str):
    # TODO we should find a way to have a global context
    ctx = ctx.parent.parent

    with SpinnerExecutor(
        ctx, "Registering account...", success_text="Registered successfully."
    ) as executor:
        register_data = ctx.client.register(username, password, email)

        ctx.cfg["state"] = CliState.LOGGED_IN
        ctx.cfg["token"] = register_data["token"]

        save_config(ctx)


@auth.command(help="Login to BBV")
@click.option("-u", "--username", help="Username", required=True)
@click.option(
    "-p",
    "--password",
    help="Password",
    prompt=True,
    hide_input=True,
    required=True,
)
@click.pass_context
def login(ctx, username, password):
    ctx = ctx.parent.parent

    with SpinnerExecutor(
        ctx, "Logging into account...", success_text="Logged in successfully."
    ) as executor:
        token = ctx.client.login(username, password)

        ctx.cfg["state"] = CliState.LOGGED_IN
        ctx.cfg["token"] = token
        save_config(ctx)


@auth.command(help="Reset BBV password")
@click.option("-u", "--username", help="Username", required=True)
@click.option("-e", "--email", help="Email", required=True)
@click.pass_context
def reset(ctx, username, email):
    ctx = ctx.parent.parent

    with SpinnerExecutor(
        ctx,
        "Initiating password reset...",
        success_text="Successfully initiated password reset, check your email!",
    ) as executor:
        ctx.client.reset_password_start(username, email)

    token = click.prompt(text="Reset token")
    new_password = click.prompt(text="New password", hide_input=True)

    with SpinnerExecutor(
        ctx,
        "Finalizing password reset...",
        success_text="Successfully reset password, please log in!",
    ) as executor:
        ctx.client.reset_password_finish(token, new_password)
