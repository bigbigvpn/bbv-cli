import httpx
from typing import Optional


class ClientError(Exception):
    pass


class Client:
    """BBV API client."""

    def __init__(
        self,
        base_url: Optional[str],
        token: Optional[str],
        api_version: str = "dev",
    ):
        self.token = token
        self.base_url = base_url
        self.api_version = api_version

    @property
    def full_api_path(self):
        return f"{self.base_url}/api/{self.api_version}"

    def _execute(self, method, path, *args, **kwargs):
        assert self.base_url is not None

        if kwargs.get("do_token", True):
            # TODO convert this error into its own exception?
            if self.token is None:
                raise ClientError("No token was provided.")
            assert self.token is not None
            headers = kwargs.get("headers", {})
            headers["Authorization"] = self.token
            kwargs["headers"] = headers

        # don't pass do_token to httpx
        if "do_token" in kwargs:
            kwargs.pop("do_token")

        resp = httpx.request(method, f"{self.full_api_path}{path}", *args, **kwargs)

        # TODO map more errors
        if resp.status_code not in [200, 204]:
            data = resp.read()
            raise ClientError(f"server gave status={resp.status_code}, data={data}")

        return resp

    def _post(self, *args, **kwargs):
        return self._execute("POST", *args, **kwargs)

    def _get(self, *args, **kwargs):
        return self._execute("GET", *args, **kwargs)

    def _patch(self, *args, **kwargs):
        return self._execute("PATCH", *args, **kwargs)

    def _delete(self, *args, **kwargs):
        return self._execute("DELETE", *args, **kwargs)

    def _put(self, *args, **kwargs):
        return self._execute("PUT", *args, **kwargs)

    def register(self, username: str, password: str, email: str):
        resp = self._post(
            "/auth/register",
            do_token=False,
            json={"username": username, "password": password, "email": email},
        )

        return resp.json()

    def login(self, username: str, password: str) -> str:
        resp = self._post(
            "/auth/login",
            do_token=False,
            json={"username": username, "password": password},
        )

        return resp.json()["token"]

    def reset_password_start(self, username: str, email: str) -> str:
        resp = self._post(
            "/auth/reset_password/start",
            do_token=False,
            json={"username": username, "email": email},
        )

        return resp.json()

    def reset_password_finish(self, reset_token: str, new_password: str):
        resp = self._post(
            "/auth/reset_password/finish",
            do_token=False,
            json={"reset_token": reset_token, "new_password": new_password},
        )

        # Only return JSON in case of errors
        return resp.json() if resp.status_code != 204 else None

    def myself(self) -> dict:
        """Return self user."""
        return self._get("/user/@me").json()

    def list_wg_keys(self) -> list:
        """Return keys owned by the user."""
        return self._get("/user/@me/keys").json()

    def add_wg_key(self, pubkey, password) -> list:
        """Add a wireguard key."""
        return self._put(
            "/user/@me/keys",
            json={"wireguard_key": pubkey, "password": password},
        ).json()

    def rm_wg_key(self, pubkey, password) -> list:
        """Remove a wireguard key."""
        return self._delete(
            "/user/@me/keys",
            json={"wireguard_key": pubkey, "password": password},
        ).json()

    def create_group(self, name) -> list:
        """Create a group."""
        return self._post(
            "/groups/",
            json={"name": name},
        ).json()

    def delete_group(self, group_id, password) -> list:
        """Delete a group."""
        return self._delete(f"/groups/{group_id}", json={"password": password}).json()

    def list_groups(self) -> list:
        """List all groups that user is a part of."""
        return self._get(
            "/groups/list",
        ).json()

    def fetch_group(self, group_id: str) -> list:
        """Fetch group info of a group that user is a part of."""
        return self._get(
            f"/groups/{group_id}",
        ).json()

    def remove_from_group(self, group_id, user_id) -> list:
        """Remove someone from a group."""
        return self._delete(f"/groups/{group_id}/members/{user_id}").json()

    def leave_group(self, group_id) -> list:
        """Leave a group."""
        return self.remove_from_group(group_id, "@me")

    def create_invite(self, group_id, max_uses, expires_at) -> list:
        """Create an invite in a group."""
        return self._put(
            f"/groups/{group_id}/invites",
            json={"max_uses": max_uses, "expires_at": expires_at},
        ).json()

    def list_invites(self, group_id) -> list:
        """Create an invite in a group."""
        return self._get(
            f"/groups/{group_id}/invites",
        ).json()

    def delete_invite(self, group_id, invite_code) -> list:
        """Create an invite in a group."""
        return self._delete(
            f"/groups/{group_id}/invites/{invite_code}",
        ).json()

    def use_invite(self, invite_code) -> list:
        """Use an invite."""
        return self._post(
            f"/invites/{invite_code}/use",
        ).json()

    def fetch_cloud_tokens(self, group) -> list:
        """Fetch cloud provider tokens of a group."""
        return self._get(
            f"/groups/{group}/cloud_tokens",
        ).json()

    def set_cloud_token(self, group, type, value) -> list:
        """Set a cloud provider token of a group."""
        return self._put(
            f"/groups/{group}/cloud_tokens/{type}", json={"value": value}
        ).json()

    def unset_cloud_token(self, group, type) -> list:
        """Unset a cloud provider token of a group."""
        return self._delete(
            f"/groups/{group}/cloud_tokens/{type}",
        ).json()

    def summon_server(self, group, provider, region, wg_port) -> list:
        """Summon a server."""
        return self._post(
            f"/groups/{group}/servers",
            timeout=60,
            json={
                "cloud_provider": provider,
                "cloud_region": region,
                "wireguard_port": wg_port,
            },
        ).json()

    def unsummon_server(self, group, server_id) -> list:
        """Unsummon server."""
        return self._delete(
            f"/groups/{group}/servers/{server_id}",
        ).json()

    def fetch_servers(self, group) -> list:
        """Fetch server list of a group."""
        return self._get(f"/groups/{group}/servers").json()

    def fetch_server(self, group, server_id) -> list:
        """Fetch a server's info by UUID."""
        return self._get(f"/groups/{group}/servers/{server_id}").json()
